import { Glyph } from "./glyph";
export type Font = {
    h: number;
    pd: {
        [key: string]: number[];
    };
    g: Glyph[];
};
