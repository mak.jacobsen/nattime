export declare enum MinuteMarkerType {
    none = "NONE",
    line_0_1 = "LINE_0_1",
    line_0_2 = "LINE_0_2",
    line_0_3 = "LINE_0_3",
    line_1_2 = "LINE_1_2",
    line_1_3 = "LINE_1_3",
    line_2_3 = "LINE_2_3",
    dot_0_1 = "DOT_0_1",
    dot_1_2 = "DOT_1_2",
    dot_2_3 = "DOT_2_3"
}
