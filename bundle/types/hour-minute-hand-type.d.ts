export declare enum HourMinuteHandType {
    a1 = "A_1",
    a2 = "A_2",
    b1 = "B_1",
    b2 = "B_2",
    c1 = "C_1",
    c2 = "C_2",
    d1 = "D_1",
    d2 = "D_2",
    e1 = "E_1",
    e2 = "E_2"
}
