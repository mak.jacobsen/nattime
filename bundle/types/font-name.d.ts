export declare enum FontName {
    bigShouldersDisplay = "BIG_SHOULDERS_DISPLAY",
    bokor = "BOKOR",
    glassAntiqua = "GLASS_ANTIQUA",
    imbue = "IMBUE",
    libreBaskerville = "LIBRE_BASKERVILLE",
    orbitron = "ORBITRON",
    voltaire = "VOLTAIRE",
    warnes = "WARNES"
}
