export declare enum CircleType {
    none = "NONE",
    thin = "THIN",
    medium = "MEDIUM",
    thick = "THICK"
}
