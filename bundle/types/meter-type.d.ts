export declare enum MeterType {
    daysOfWeek = "DAYS_OF_WEEK_METER_TYPE",
    amPm = "AM_PM_METER_TYPE"
}
