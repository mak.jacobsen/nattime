export declare enum HourNumeralDisplay {
    none = "NONE",
    quaterNumeralDisplay = "QUATER_NUMERAL_DISPLAY",
    quaterNumeralDisplayWithLogo = "QUATER_NUMERAL_DISPLAY_WITH_LOGO",
    halfNumeralDisplay = "HALF_NUMERAL_DISPLAY",
    halfNumeralDisplayWithLogo = "HALF_NUMERAL_DISPLAY_WITH_LOGO",
    fullNumeralDisplay = "FULL_NUMERAL_DISPLAY",
    fullNumeralDisplayWithLogo = "FULL_NUMERAL_DISPLAY_WITH_LOGO"
}
