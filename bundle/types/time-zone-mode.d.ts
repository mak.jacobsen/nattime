export declare enum TimeZoneMode {
    local = "Local",
    utc = "UTC"
}
