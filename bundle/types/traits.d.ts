import { AmPmType } from "./am-pm-type";
import { CircleType } from "./circle-type";
import { DaysOfWeekType } from "./days-of-week-type";
import { FontName } from "./font-name";
import { HourMinuteHandType } from "./hour-minute-hand-type";
import { HourMarkerType } from "./hour-marker-type";
import { HourNumeralDisplay } from "./hour-numeral-display";
import { HourNumeralSystem } from "./hour-numeral-system";
import { MinuteMarkerType } from "./minute-marker-type";
import { NumberSize } from "./number-size";
import { SecondHandType } from "./second-hand-type";
export interface Traits {
    outerCircleType: CircleType;
    middleCircleType: CircleType;
    innerCircleType: CircleType;
    hourMinuteHandType: HourMinuteHandType;
    secondHandType: SecondHandType;
    hourMarkerType: HourMarkerType;
    minuteMarkerType: MinuteMarkerType;
    numberFont: FontName;
    numberSize: NumberSize;
    hasRotatedNumbers: boolean;
    hourNumeralSystem: HourNumeralSystem;
    hourNumeralDisplay: HourNumeralDisplay;
    amPmType: AmPmType;
    daysOfWeekType: DaysOfWeekType;
}
