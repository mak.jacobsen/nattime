import { Traits } from "./traits";
export interface Platform {
    message: string;
    signature: string;
}
export interface Options {
    blockHeight?: number;
    traits?: Traits;
    showRadialCircles?: boolean;
    platform?: Platform;
}
