export declare enum ColorHex {
    black = "#000000",
    orange = "#F7921D",
    red = "#E34427",
    white = "#FFFFFF"
}
