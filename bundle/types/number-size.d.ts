export declare enum NumberSize {
    small = "SMALL",
    medium = "MEDIUM",
    large = "LARGE"
}
