import { BlockInfo } from "./block-info";
import { Options } from "./options";
export interface InitInfo extends Options {
    blockInfo: BlockInfo;
}
