export type Glyph = {
    c: string;
    p: string;
    m: {
        w: number;
    };
};
